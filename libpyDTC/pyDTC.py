# Developer: Alexander Ruede (CERN/KIT-IPE), Yiannis Kazas (CERN/NCSR Demokritos)

# Python API for CMS Inner Tracker FC7-based RD53A test setup.
# Further reading about the uHAL API for IPbus communication:
# https://ipbus.web.cern.ch/ipbus/doc/user/html/software/uhalQuickTutorial.html

from __future__ import division, print_function, absolute_import

import sys
import uhal
from time import sleep
import time
import os

try:
    from . import it_fast_cmd_cnfg as cnfg
except ValueError:
    import it_fast_cmd_cnfg as cnfg

import json
import pprint
from collections import OrderedDict

import math

# mimics uhal::Node and adds logging
class ulog_node:
    def __init__(self, ulog, name):
        self.name = name
        self.node = ulog.hw.getNode(name)
        self.ulog = ulog

    def read(self):
        return self.node.read()

    def write(self, value):
        self.ulog.log_file.write("%s = %s\n" % (self.name, value))
        return self.node.write(value)

    def readBlock(self, n):
        return self.node.readBlock(n)

    def readBlockOffset(self, n, offset):
        return self.node.readBlockOffset(n, offset)

    def getSize(self):
        return self.node.getSize()

# mimics uhal::HwInterface and adds logging
class ulog:
    def __init__(self, hw):
        self.hw = hw
        self.log_file = open("ipb_log.txt", "w+")

    def __del__(self):
        print("Saving ipb_log.txt")
        self.log_file.close()

    def getNode(self, name):
        return ulog_node(self, name)

    def dispatch(self):
        return self.hw.dispatch()


class pyDTC:
    def __init__( self, ipaddr ):
        uhal.setLogLevelTo( uhal.LogLevel.WARNING )
        self.ipaddr = ipaddr
        self.hw = ulog(uhal.getDevice( "FC7" , "ipbusudp-2.0://" + self.ipaddr + ":50001", "file://" + os.path.join(os.path.dirname(__file__), "cfg/device_address_table_fc7.xml") )) # ulog it
        # self.Nb_modules  = self.hw.getNode("user.stat_regs.aurora_rx.Nb_of_modules").read()
        # self.Module_type = self.hw.getNode("user.stat_regs.aurora_rx.Module_type").read()
        self.n_ch = self.hw.getNode("user.stat_regs.aurora_rx.Nb_of_modules").read()#self.Nb_modules * self.Module_type #self.hw.getNode("user.stat_regs.aurora_rx.Nb_of_modules").read()
        self.board_option_map = self.hw.getNode("user.stat_regs.aurora_rx.speed").read()
        self.hw.dispatch()
        # print(self.n_ch.value())

    # # ---- Command Encoding -------------------------#
    # ECR_CMD = 0x5A5A
    # BCR_CMD = 0x5959
    # GP_CMD  = 0x5C5C
    # CAL_CMD = 0x6363
    # WR_REG_CMD = 0x6666
    # RD_REG_CMD = 0x6565
    # SYNC_CMD   = 0x817E

    # # ---- Data Encoding ----------------------------#
    # D0 = 0x6A
    # D1 = 0x6C
    # D2 = 0x71
    # D3 = 0x72
    # D4 = 0x74
    # D5 = 0x8B
    # D6 = 0x8D
    # D7 = 0x8E
    # D8 = 0x93
    # D9 = 0x95
    # D10 = 0x96
    # D11 = 0x99
    # D12 = 0x9A
    # D13 = 0x9C
    # D14 = 0xA3
    # D15 = 0xA5
    # D16 = 0xA6
    # D17 = 0xA9
    # D18 = 0xAA
    # D19 = 0xAC
    # D20 = 0xB1
    # D21 = 0xB2
    # D22 = 0xB4
    # D23 = 0xC3
    # D24 = 0xC5
    # D25 = 0xC6
    # D26 = 0xC9
    # D27 = 0xCA
    # D28 = 0xCC
    # D29 = 0xD1
    # D30 = 0xD2
    # D31 = 0xD4

    def encode_fields(self, fields):
        value_map = {
            0: 0b01101010,
            1: 0b01101100,
            2: 0b01110001,
            3: 0b01110010,
            4: 0b01110100,
            5: 0b10001011,
            6: 0b10001101,
            7: 0b10001110,
            8: 0b10010011,
            9: 0b10010101,
            10: 0b10010110,
            11: 0b10011001,
            12: 0b10011010,
            13: 0b10011100,
            14: 0b10100011,
            15: 0b10100101,
            16: 0b10100110,
            17: 0b10101001,
            18: 0b10101010,
            19: 0b10101100,
            20: 0b10110001,
            21: 0b10110010,
            22: 0b10110100,
            23: 0b11000011,
            24: 0b11000101,
            25: 0b11000110,
            26: 0b11001001,
            27: 0b11001010,
            28: 0b11001100,
            29: 0b11010001,
            30: 0b11010010,
            31: 0b11010100
        }
        result = []
        for f in fields:
            result.append(value_map[f])
        return result


    def write_chip_command(self, cmd, fields=[]):
        n_words = int(math.ceil((16 + len(fields) * 8) // 32.0))
        self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(0xFff00000 | (0xF << 16) | (n_words))# << 1))
        if len(fields) == 0:
            self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(cmd | 0b1000000101111110)
        else:
            encoded_fields = self.encode_fields(fields)
            self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(cmd << 24 | cmd << 16 | encoded_fields[0] << 8 | encoded_fields[1])
            n_fields_done = 2
            for i in range(1, n_words):
                data = 0b10000001011111101000000101111110
                # while n_fields_done != len(fields):
                for j, k in enumerate(range(n_fields_done, len(fields))):
                    data = data & ~(0xFF << (24 - 8 *j)) | (encoded_fields[k] << (24 - 8 * j))
                    n_fields_done += 1
                self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(data)
        self.hw.getNode("user.ctrl_regs.Slow_cmd.dispatch_packet").write(1)
        self.hw.getNode("user.ctrl_regs.Slow_cmd.dispatch_packet").write(0)
        self.hw.dispatch()
        # print("data=", fields)
        # sleep(0.001)
        # self.slow_cmd_status()

    # def write_chip_command(self, cmd, fields=[]):
    #     n_words = int(math.ceil((16 + len(fields) * 8) / 32.0))
    #     cmd_data = []
    #     # self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(0xFff00000 | (0xF << 16) | (n_words))# << 1))
    #     cmd_data.append(0xFff00000 | (0xF << 16) | (n_words))
    #     if len(fields) == 0:
    #         self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(cmd | 0b1000000101111110)
    #     else:
    #         encoded_fields = self.encode_fields(fields)
    #         # self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(cmd << 24 | cmd << 16 | encoded_fields[0] << 8 | encoded_fields[1])
    #         cmd_data.append(cmd << 24 | cmd << 16 | encoded_fields[0] << 8 | encoded_fields[1])
    #         n_fields_done = 2
    #         for i in range(1, n_words):
    #             data = 0b10000001011111101000000101111110
    #             # while n_fields_done != len(fields):
    #             for j, k in enumerate(range(n_fields_done, len(fields))):
    #                 data = data & ~(0xFF << (24 - 8 *j)) | (encoded_fields[k] << (24 - 8 * j))
    #                 n_fields_done += 1
    #             # self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(data)
    #             cmd_data.append(data)
    #     for word in cmd_data:
    #         print(hex(word))
    #         self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(word)
    #     self.hw.dispatch()


    def write_ecr(self, write=1):
        self.write_chip_command(0b01011010)

    def write_register( self, address, data, chip_id=15, write=1 ):
        self.write_chip_command(
            0b01100110,
            [
                chip_id << 1,
                address >> 4,
                ((address & 0xf) << 1) + (data >> 15),
                (data >> 10) & 0b11111,
                (data >> 5) & 0b11111,
                data & 0b11111
            ]
        )

    def send_read_register_cmd( self, address, data, chip_id=15, write=1 ):
        self.write_chip_command(
            0b01100101,
            [
                chip_id << 1,
                address >> 4,
                ((address & 0xf) << 1) + (data >> 15),
                0
            ]
        )

    def write_global_pulse( self, width=3, chip_id=15, write=1 ):
        self.write_chip_command(
            0b01011100,
            [
                chip_id << 1,
                width << 1
            ]
        )

    def test_write (self):
        self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(0xFfff0004) #Header
        self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(0x6666D272)
        self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(0xD26A6A6c)#6c #72
        # self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(0x5A5A817e)
        # self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(0x5A5A817e)
        # self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(0xF0010004) #Header
        # self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(0x6666D272)
        # self.hw.getNode("user.ctrl_regs.Slow_cmd_fifo_din").write(0xD26A6A72)#6c #72
        self.hw.dispatch()


    def slow_cmd_status (self):
        empty_flag = self.hw.getNode("user.stat_regs.slow_cmd.fifo_empty").read()
        full_flag  = self.hw.getNode("user.stat_regs.slow_cmd.fifo_full").read()
        error_flag = self.hw.getNode("user.stat_regs.slow_cmd.error_flag").read()
        packet_ok  = self.hw.getNode("user.stat_regs.slow_cmd.fifo_packet_dispatched").read()
        self.hw.dispatch()
        print("fifo empty =", empty_flag)
        print("fifo full  =", full_flag)
        print("fifo error =", error_flag)
        print("packet ok  =", packet_ok)

    def slow_cmd_rst (self):
        self.hw.getNode("user.ctrl_regs.Slow_cmd.fifo_reset").write(1) #Set Reset
        self.hw.getNode("user.ctrl_regs.Slow_cmd.fifo_reset").write(0) #Release Reset
        self.hw.dispatch()

    # ---- IPbus ------------------------------------#
    def ipb_read( self, nodeID, dispatch=1 ):
        word = self.hw.getNode(nodeID).read()
        if dispatch == 1:
            self.hw.dispatch()
            print(nodeID, "=", word)
        return int(word)

    def ipb_write( self, nodeID, data, dispatch=1 ):
        self.hw.getNode(nodeID).write(data)
        if dispatch == 1:
            self.hw.dispatch()
        return int(data)

    def ipb_dispatch( self ):
        return self.hw.dispatch()
    #------------------------------------------------#

    def fmc_pwr_on( self ):
        self.hw.getNode("system.ctrl_2.fmc_l12_pwr_en").write(1)
        self.hw.getNode("system.ctrl_2.fmc_l8_pwr_en").write(1)
        self.hw.getNode("system.ctrl_2.fmc_pg_c2m").write(1)
        self.hw.dispatch()

    def fmc_pwr_off( self ):
        self.hw.getNode("system.ctrl_2.fmc_l12_pwr_en").write(0)
        self.hw.getNode("system.ctrl_2.fmc_l8_pwr_en").write(0)
        self.hw.getNode("system.ctrl_2.fmc_pg_c2m").write(0)
        self.hw.dispatch()

    def set_reset_all( self ):
        self.hw.getNode("user.ctrl_regs.reset_reg.aurora_rst").write(0) #ACTIVE LOW!
        self.hw.dispatch()
        sleep(0.2)
        self.hw.getNode("user.ctrl_regs.reset_reg.aurora_pma_rst").write(0) #ACTIVE LOW!
        self.hw.dispatch()
        sleep(0.2)
        self.hw.getNode("user.ctrl_regs.reset_reg.global_rst").write(1)
        # self.hw.getNode("user.ctrl_regs.reset_reg.clk_gen_rst").write(1)
        self.hw.getNode("user.ctrl_regs.reset_reg.fmc_pll_rst").write(0) #INVERTED!
        self.hw.getNode("user.ctrl_regs.reset_reg.cmd_rst").write(1)
        self.hw.getNode("user.ctrl_regs.reset_reg.i2c_rst").write(1)
        self.hw.getNode("user.ctrl_regs.Register_RdBack.fifo_reset").write(1)
        self.hw.getNode("user.ctrl_regs.Optical_link.gbtbank_reset").write(1)
        self.hw.dispatch()

    def release_reset_all( self ):
        self.hw.getNode("user.ctrl_regs.reset_reg.global_rst").write(0)
        self.hw.getNode("user.ctrl_regs.reset_reg.clk_gen_rst").write(0)
        self.hw.dispatch()
        sleep(0.4)
        self.hw.getNode("user.ctrl_regs.reset_reg.fmc_pll_rst").write(1) #INVERTED!
        self.hw.getNode("user.ctrl_regs.reset_reg.cmd_rst").write(0)
        self.hw.getNode("user.ctrl_regs.Register_RdBack.fifo_reset").write(0)
        self.hw.getNode("user.ctrl_regs.Optical_link.gbtbank_reset").write(0)
        self.hw.dispatch()
        sleep(0.4)
        self.hw.getNode("user.ctrl_regs.reset_reg.i2c_rst").write(0)
        self.hw.dispatch()
        sleep(0.6)
        self.hw.getNode("user.ctrl_regs.reset_reg.aurora_pma_rst").write(1) #ACTIVE LOW!
        self.hw.dispatch()
        sleep(0.4)
        self.hw.getNode("user.ctrl_regs.reset_reg.aurora_rst").write(1) #ACTIVE LOW!
        self.hw.dispatch()
        sleep(0.2)

    def reset_setup( self ):
        print("Resetting...")
        self.fmc_pwr_on()
        self.set_reset_all()
        sleep(0.2)
        self.release_reset_all()
        sleep(0.2)
        print("Reset DONE.")
        print("Powercycle SCC and continue with INIT!")


    def init_aurora( self, lanes=1 ):
        if lanes == 1:
            self.write_register(61, int("0b00000100",2)) ## OUTPUT_CONFIG
            self.write_register(69, int("0b00000001",2)) ## CML_CONFIG
            CB_Wait = 255
            CB_Send = 1
            self.write_register(74,((CB_Wait << 4) | CB_Send & 0x0f) & 0xff) ## AURORA_CB_CONFIG0
            self.write_register(75,(CB_Wait & 0xfffff) >> 4) ## AURORA_CB_CONFIG1
        self.write_register(44, 0x30)
        self.write_global_pulse( width=1 )
        self.write_global_pulse( width=1 )
        sleep(0.05)

    def enable_monitor( self ):
    # enable_monitor needed in order to receive register frames
        self.write_register(44, 0x0100)
        self.write_global_pulse( width=4 )
        link = self.board_option_map
    # CDR_CONFIG (64) register for aurora 640Mbps
        if link == 1:
            print(("Aurora receiver running at 640Mb/s"))
            self.write_register(64, int('0b'
                +'0' # CDR_SEL_DEL_CLK
                +'00' # CDR_PD_SEL[1:0]
                +'0100' # CDR_PD_DEL[3:0]
                +'0' # CDR_EN_GCK2
                +'011' # CDR_VCO_GAIN[2:0]
                +'001' # CDR_SEL_SER_CLK[2:0]
                , 2))
        else:
            print(("Aurora receiver running at 1.28Gb/s"))

    def init_chip( self, iterations=1 ):
        for _ in range(iterations):
            self.write_ecr()
        self.init_aurora()
        self.enable_monitor()

    def init_setup( self, iterations=30 ):
        print("Initializing...")
        chnl_up = 0
        i = 0
        self.init_chip()
        while( chnl_up != (2**self.n_ch)-1 ):
            self.init_chip()
            sleep(0.01)
            chnl_up = self.ipb_read("user.stat_regs.aurora_rx_channel_up")
            i += 1
            if i > iterations:
                self.status()
                raise RuntimeError("Aurora channel up TIMEOUT! Check electrical connections, powercycle SCC or reset firmware and try again.")
        print("Initialization DONE")
        self.status()


    def status( self ):
        i2c_init = self.hw.getNode("user.stat_regs.global_reg.i2c_init").read()
        clk_gen_lock = self.hw.getNode("user.stat_regs.global_reg.clk_gen_lock").read()
        gtx_lock = self.hw.getNode("user.stat_regs.aurora_rx_gt_locked").read()
        lane_up = self.hw.getNode("user.stat_regs.aurora_rx.lane_up").read()
        channel_up = self.hw.getNode("user.stat_regs.aurora_rx_channel_up").read()
        gt_ref_clk = self.hw.getNode("user.stat_regs.stat_reg_22").read()
        link = self.board_option_map
        self.hw.dispatch()
        print("------------------------------------------------")
        print("i2c_init          ", i2c_init)
        print("clk_gen_lock      ", clk_gen_lock)
        print("gtx_lock          ", bin(int(gtx_lock)))
        print("link aurora          ", link)
        print("lane_up           ", bin(int(lane_up)))
        print("channel_up        ", bin(int(channel_up)))
        print("------------------------------------------------")
        print("gt_ref_clk        ", int(gt_ref_clk)//1000.0)
        print("------------------------------------------------")
        print("")
        # if channel_up != (2**self.n_ch)-1:
        #     raise RuntimeWarning("Not all Aurora channels are up!")
        #if int(gt_ref_clk)/1000.0 < 160.3 or int(gt_ref_clk)/1000.0 > 160.33:
            #raise RuntimeWarning("GTX reference clock out of range: %f")% (int(gt_ref_clk)/1000.0)

    # Attempt to configure the lin FE
    def config_linear_fe( self ):
        # Digital Matrix linear FE
        self.write_register(32, 0)#0xffff)
        self.write_register(33, 0xffff)#0b1000000000000000)
        self.write_register(34, 0)
        self.write_register(35, 0)
        self.write_register(36, 0)
        # Linear FE config
        self.write_register(14, 100)
        self.write_register(15, 20)
        self.write_register(16, 50)
        self.write_register(17, 20)
        self.write_register(18, 110)
        # REF_KRUM_LIN
        self.write_register(19, 300)
        # Global threshold (> REF_KRUM_LIN)
        self.write_register(20, 330) #330

        # CAL_COLPR
        self.write_register(46, 0xFFFF)
        self.write_register(47, 0xFFFF)
        self.write_register(48, 0xFFFF)
        self.write_register(49, 0xFFFF)
        self.write_register(50, 0xFFFF)
        self.write_register(51, 0xFFFF)
        self.write_register(52, 0xFFFF)
        self.write_register(53, 0xFFFF)
        self.write_register(54, 0xF)
        self.write_register(55, 0xFFFF)
        self.write_register(56, 0xFFFF)
        self.write_register(57, 0xFFFF)
        self.write_register(58, 0xFFFF)
        self.write_register(59, 0xF)


        self.write_register(63, 0)

        # VCAL_HIGH
        self.write_register(41, 900)

        # INJECTION_SELECT
        self.write_register(39, 0)

        # PIX_DEFAULT_CONFIG
        # self.write_register(4, 0x9ce2)

        # LATENCY_CONFIG
        self.write_register(37, 40)

        # SER_SEL_OUT
        # self.write_register(68, 0b01010101)#0b10101010)

        # CML_CONFIG
        # self.write_register(69, 1)

        # pass

        # diff
        # self.write_register(21, 895) # PRMP_DIFF
        # self.write_register(22, 542) # FOL_DIFF
        # self.write_register(23, 512) # PRECOMP_DIFF
        # self.write_register(24, 528) # COMP_DIFF
        # self.write_register(25, 89) # VFF_DIFF
        # self.write_register(26, 50) # VTH1_DIFF
        # self.write_register(27, 0) # VTH2_DIFF
        # self.write_register(28, 20) # LCC_DIFF
        # self.write_register(29, 0) # CONF_FE_DIFF



    # ---- Fast Command Block ------------------------------------#
    # Author: Ioannis Kazas
    # ------------------------------------------------------------#

    # Set Initial Configuration
    def cnfg_fast_cmds( self ):
        # Set Configuration Registers
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_2.trigger_source").write(cnfg.TRIGGER_SOURCE)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_2.autozero_source").write(cnfg.AUTOZERO_SOURCE)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_2.backpressure_en").write(cnfg.BACKPRESSURE_EN)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_2.init_ecr_en").write(cnfg.INIT_ECR_EN)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_2.veto_en").write(cnfg.VETO_EN)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_2.tp_fsm_ecr_en").write(cnfg.TP_FSM_ECR_EN)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_2.tp_fsm_test_pulse_en").write(cnfg.TP_FSM_TEST_PULSE_EN)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_2.tp_fsm_inject_pulse_en").write(cnfg.TP_FSM_INJECT_PULSE_EN)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_2.tp_fsm_trigger_en").write(cnfg.TP_FSM_TRIGGER_EN)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_3.triggers_to_accept").write(cnfg.TRIGGERS_TO_ACCEPT)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_3.delay_after_ecr").write(cnfg.DELAY_AFTER_ECR)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_4.cal_data_prime").write(cnfg.CAL_DATA_PRIME)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_4.delay_after_prime_pulse").write(cnfg.DELAY_AFTER_PRIME_PULSE)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_5.cal_data_inject").write(cnfg.CAL_DATA_INJECT)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_5.delay_after_inject_pulse").write(cnfg.DELAY_AFTER_INJECT_PULSE)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_6.delay_after_autozero").write(cnfg.DELAY_AFTER_AUTOZERO)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_6.delay_before_next_pulse").write(cnfg.DELAY_BEFORE_NEXT_PULSE)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_7.glb_pulse_data").write(cnfg.GLB_PULSE_DATA)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_7.autozero_freq").write(cnfg.AUTOZERO_FREQ)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_7.veto_after_autozero").write(cnfg.VETO_AFTER_AUTOZERO)

        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_2.trigger_duration").write(cnfg.TRIGGER_DURATION)

        self.hw.dispatch()
        # Set "cmd_strobe" and "load_config" to force fw load configuration
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.load_config").write(1)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.cmd_strobe").write(1)
        self.hw.dispatch()
        # Clear "cmd_strobe" and "load_config" bits
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.cmd_strobe").write(0)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.load_config").write(0)
        self.hw.dispatch()
        print("Fast Commands Block Configured")

    def start_trig_process( self ):
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.start_trigger").write(1)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.cmd_strobe").write(1)
        # self.hw.dispatch()
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.cmd_strobe").write(0)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.start_trigger").write(0)
        self.hw.dispatch()

    def reset_fast_cmds( self ):
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.ipb_reset").write(1)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.cmd_strobe").write(1)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.cmd_strobe").write(0)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.ipb_reset").write(0)
        self.hw.dispatch()

    def stop_trig_process( self ):
        # if write == 1:
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.stop_trigger").write(1)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.cmd_strobe").write(1)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.cmd_strobe").write(0)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.stop_trigger").write(0)
        self.hw.dispatch()

    def send_fast_ecr( self ):
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.ipb_ecr").write(1)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.ipb_ecr").write(0)
        self.hw.dispatch()

    def send_fast_bcr( self ):
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.ipb_bcr").write(1)
        self.hw.getNode("user.ctrl_regs.fast_cmd_reg_1.ipb_bcr").write(0)
        self.hw.dispatch()

    def fast_cmd_status( self ):
        trigger_source_o = self.hw.getNode("user.stat_regs.fast_cmd.trigger_source_o").read()
        trigger_state = self.hw.getNode("user.stat_regs.fast_cmd.trigger_state").read()
        if_configured = self.hw.getNode("user.stat_regs.fast_cmd.if_configured").read()
        error_code = self.hw.getNode("user.stat_regs.fast_cmd.error_code").read()
        trigger_cntr = self.hw.getNode("user.stat_regs.trigger_cntr").read()
        # trigger_tag = self.hw.getNode("user.stat_regs.trigger_tag").read()
        self.hw.dispatch()
        print("------------------------------------------------")
        print("trigger_source                   ", trigger_source_o)
        print("trigger_state                    ", trigger_state)
        print("if_configured                    ", if_configured)
        print("error_code                       ", error_code)
        print("trigger_cntr                     ", trigger_cntr)
        # print("trigger_tag                      ", trigger_tag)
        print("------------------------------------------------")
        print("")

    def ext_tlu_config(self):
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg1.dio5_en").write(cnfg.DIO5_ENABLE)
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg2.tlu_en").write(cnfg.TLU_ENABLE)
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg2.tlu_handshake_mode").write(cnfg.TLU_HNDSHAKE_MODE)

    def config_dio5(self):
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg2.ext_clk_en").write(cnfg.EXT_CLK_EN)
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg1.dio5_ch_out_en").write(cnfg.DIO5_CHOUT_EN)
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg1.dio5_term_50ohm_en").write(cnfg.DIO5_TERM_EN)
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg1.dio5_ch1_thr").write(cnfg.DIO5_CH1THR)
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg1.dio5_ch2_thr").write(cnfg.DIO5_CH2THR)
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg2.dio5_ch3_thr").write(cnfg.DIO5_CH3THR)
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg2.dio5_ch4_thr").write(cnfg.DIO5_CH4THR)
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg2.dio5_ch5_thr").write(cnfg.DIO5_CH5THR)
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg2.dio5_load_config").write(1)
        self.hw.getNode("user.ctrl_regs.ext_tlu_reg2.dio5_load_config").write(0)
        self.hw.dispatch()

    def dio5_status(self):
        dio5_not_ready =  self.hw.getNode("user.stat_regs.stat_dio5.dio5_not_ready").read()
        dio5_error     =  self.hw.getNode("user.stat_regs.stat_dio5.dio5_error").read()
        self.hw.dispatch()
        print("dio5 not ready", dio5_not_ready)
        print("dio5 error", dio5_error)


    def Reset_chip_buffers(self):
        self.hw.getNode("user.ctrl_regs.Register_RdBack.fifo_reset").write(1)
        self.hw.dispatch()
        self.hw.getNode("user.ctrl_regs.Register_RdBack.fifo_reset").write(0)
        self.hw.dispatch()

    # Select from which chip you want to read AutoRead Register Data
    def Config_autoread(self,addr_a,addr_b):
        self.hw.getNode("user.ctrl_regs.Register_RdBack.AutoRead_addr_a").write(addr_a)#(cnfg.AUTOREAD_ADDR_A)
        self.hw.getNode("user.ctrl_regs.Register_RdBack.AutoRead_addr_b").write(addr_b)#(cnfg.AUTOREAD_ADDR_B)
        self.hw.dispatch()

    # Readout Register-Readback data
    def autoread_readback(self):
        autoread_reg_a =  self.hw.getNode("user.stat_regs.AutoRead_Reg_A").read()
        autoread_reg_b =  self.hw.getNode("user.stat_regs.AutoRead_Reg_B").read()
        self.hw.dispatch()
        print("Auto-Read A Register Data =", autoread_reg_a)
        print("Auto-Read B Register Data =", autoread_reg_b)

    def Rdback_fifo_status(self):
        Reg_fifo_full =  self.hw.getNode("user.stat_regs.Register_Rdback.fifo_full").read()
        Reg_fifo_empty =  self.hw.getNode("user.stat_regs.Register_Rdback.fifo_empty").read()
        self.hw.dispatch()
        print("fifo full = ", Reg_fifo_full)
        print("fifo empty = ", Reg_fifo_empty)

    def Register_readout(self):
        Reg_fifo_empty =  self.hw.getNode("user.stat_regs.Register_Rdback.fifo_empty").read()
        self.hw.dispatch()
        while Reg_fifo_empty == 0:
            Reg_data =  self.hw.getNode("user.stat_regs.Register_Rdback_fifo").read()
            self.hw.dispatch()
            Reg_fifo_empty =  self.hw.getNode("user.stat_regs.Register_Rdback.fifo_empty").read()
            self.hw.dispatch()
            print("Register Data = ", hex(Reg_data))
            sleep(0.01)

    # Select from which chip you want to read Aurora error counter
    def Config_aurora_error_cntr(self,module_addr,chip_addr):
        self.hw.getNode("user.ctrl_regs.Aurora_block.error_cntr_module_addr").write(module_addr)#(cnfg.AURORA_ERROR_MODULE_ADDR)
        self.hw.getNode("user.ctrl_regs.Aurora_block.error_cntr_chip_addr").write(chip_addr)#(cnfg.AURORA_ERROR_CHIP_ADDR)
        self.hw.dispatch()

    # Readout Register-Readback data
    def Read_aurora_error_cntr(self):
        Hard_error_cntr =  self.hw.getNode("user.stat_regs.aurora_error_counter.hard").read()
        Soft_error_cntr =  self.hw.getNode("user.stat_regs.aurora_error_counter.soft").read()
        self.hw.dispatch()
        print("Aurora Hard error counter = ", Hard_error_cntr)
        print("Aurora Soft error counter = ", Soft_error_cntr)

    # ------------------------------------------------------------#
    # -- Hit-Data Readout
    # ------------------------------------------------------------#
    def readout_block_cnfg(self):
        self.hw.getNode("user.ctrl_regs.readout_block.data_handshake_en").write(cnfg.READOUT_DATA_HANDSHAKE_EN)
        self.hw.getNode("user.ctrl_regs.readout_block.l1a_timeout_value").write(cnfg.L1A_TIMEOUT)
        self.hw.getNode("user.ctrl_regs.Hybrids_en").write(cnfg.HYBRID_ENABLE)
        self.hw.getNode("user.ctrl_regs.Chips_en").write(cnfg.CHIPS_ENABLE)
        self.hw.getNode("user.ctrl_regs.readout_block.Nb_of_events").write(cnfg.N_EVENTS_READOUT)
        self.hw.dispatch()
        print("Readout Block Configured")

    def ReadReg(self, reg_id):
        v = self.hw.getNode(reg_id).read()
        self.hw.dispatch()
        if not v.valid():
            print("error reading register: " + reg_id)
            return None
        return v.value()

    def WriteReg(self, reg_id, value):
        self.hw.getNode(reg_id).write(value)
        self.hw.dispatch()


    def ResetReadout(self):
        self.hw.getNode("user.ctrl_regs.reset_reg.readout_block_rst").write(1)
        sleep(0.001)
        self.hw.getNode("user.ctrl_regs.reset_reg.readout_block_rst").write(0)
        sleep(0.001)
        # self.hw.dispatch()

        self.ddr3_offset = 0

        init_calib_done = self.hw.getNode("user.stat_regs.readout1.ddr3_initial_calibration_done").read()
        self.hw.dispatch()
        print("initial calibration =", init_calib_done)
        while init_calib_done != 1:
            init_calib_done = self.hw.getNode("user.stat_regs.readout1.ddr3_initial_calibration_done").read()
            self.hw.dispatch()
            print("initial calibration done =", init_calib_done)
            # wait for initial calibration
            sleep(0.1)

    def decode_events(self,data):
        event_start = []
        for i in range(0, len(data), 4):
            if data[i] & 0xffff0000 == 0xffff0000:
                event_start.append(i)

        events = []

        for i in range(len(event_start)):
            start = event_start[i]
            end = len(data) if i == len(event_start) - 1 else event_start[i+1]
            evt = self.decode_header(data[start:end])
            events.append(evt)
            if (evt["_header"]["block_size"] * 4 != end - start):
                print("Decoding error: block size is %s instead of %s" % (evt["_header"]["block_size"], end - start))
                raw_input("Press Enter to continue...")

        return events


    def decode_header(self, data):
        result = { "_header" : {} }
        if (data[0] & 0xffff0000) != 0xffff0000:
            print("invalid header")
            return {}
        result["_header"]["block_size"] = data[0] & 0x0000ffff
        result["_header"]["tlu_trigger_id"] = (data[1] & 0xffff0000) >> 16
        result["_header"]["data_format_ver"] = (data[1] & 0x0000ff00) >> 8
        result["_header"]["dummy_size"] = data[1] & 0x000000ff
        result["_header"]["tdc"] = (data[2] & 0xff000000) >> 24
        result["_header"]["l1a_counter"] = data[2] & 0x00ffffff
        result["_header"]["bx_counter"] = data[3]
        result["chips"] = []

        chip_start = []
        for i in range(4, len(data), 4):
            if data[i] & 0xa0000000 == 0xa0000000:
                chip_start.append(i)

        for i in range(len(chip_start)):
            start = chip_start[i]
            end = len(data) if i == len(chip_start) - 1 else chip_start[i+1]

            result["chips"].append({ "_header" : {} })
            result["chips"][-1]["_header"]["error_code"] = (data[start] & 0x0f000000) >> 24
            result["chips"][-1]["_header"]["hybrid_id"] = (data[start] & 0x00ff0000) >> 16
            result["chips"][-1]["_header"]["chip_id"] = (data[start] & 0x0000f000) >> 12
            result["chips"][-1]["_header"]["l1a_data_size"] = data[start] & 0x00000fff
            result["chips"][-1]["_header"]["chip_type"] = (data[start + 1] & 0x0000f000) >> 12
            result["chips"][-1]["_header"]["frame_delay"] = data[start + 1] & 0x00000fff

            result["chips"][-1]["_header"]["trigger_id"] = (data[start + 2] >> 20) & 0x1F
            result["chips"][-1]["_header"]["trigger_tag"] = (data[start + 2]  >> 15) & 0x1F
            result["chips"][-1]["_header"]["bc_id"] = data[start + 2] & 0x7FFF


            result["chips"][-1]["data"] = []

            for j in range(start + 3, end):
                if (data[j] != 0):
                    region_data = OrderedDict([
                        ("col" , (((data[j] >> 26) << 1) | (data[j] >> 16) & 1) * 4),
                        ("row" , (data[j] >> 17) & 0x1ff),
                        ("ToTs" , [(data[j] >> (k * 4)) & 0xf for k in range(3, -1, -1)])
                    ])
                    if (region_data["col"] > 399 or region_data["row"] > 191):
                        print("Wrong region data.", result["_header"]["l1a_counter"], region_data, result["chips"][-1]["_header"]["trigger_id"])
                        raw_input("Press any key.")

                    result["chips"][-1]["data"].append(json.dumps(region_data))

        return result


    def ReadData(self,wait=False):#(wait=False):
        cEventSize = 1 # <---------------------

        # cNWords = self.ReadReg("user.stat_regs.words_to_read")
        cNWords = self.hw.getNode("user.stat_regs.words_to_read").read()
        data_handshake = self.hw.getNode("user.ctrl_regs.readout_block.data_handshake_en").read()
        # cPackageSize = self.ReadReg("fc7_daq_cnfg.readout_block.packet_nbr") + 1
        cNtriggers = self.hw.getNode("user.stat_regs.trigger_cntr").read()
        self.hw.dispatch()
        print("-----------------------------")
        print("Nb of words to read   =", cNWords )
        print("Data Handshake Enable =", data_handshake)
        print("Trigger counter       =", cNtriggers)
        print("-----------------------------")
        print(" ")

        data = []

        if cNWords == 0 and not wait:
            return []
        while cNWords == 0:
            sleep(0.001)
            cNWords = self.hw.getNode("user.stat_regs.words_to_read").read()
            FSM_stat = self.hw.getNode("user.stat_regs.readout1.fsm_status").read()
            req_appr = self.hw.getNode("user.stat_regs.stat_reg_29").read()
            self.hw.dispatch()
            print("FSM Status         ", FSM_stat)
            print("Nb of words to read", cNWords)
            print("Request approved   ", req_appr)
            print(" ")

        if data_handshake:
            # cNWords = self.ReadReg("fc7_daq_stat.readout_block.general.words_cnt")
            # cNtriggers = self.ReadReg("fc7_daq_stat.fast_command_block.trigger_in_counter")
            cNWords = self.hw.getNode("user.stat_regs.words_to_read").read()
            cReadoutReq = self.hw.getNode("user.stat_regs.readout1.readout_req").read()
            self.hw.dispatch()
            print("Readout Request =", cReadoutReq)
            print("Number of words with habdshake ", cNWords)

            while cReadoutReq == 0:
                cReadoutReq = self.hw.getNode("user.stat_regs.readout1.readout_req").read()
                FSM_stat = self.hw.getNode("user.stat_regs.readout1.fsm_status").read()
                self.hw.dispatch()
                print("FSM Status      =", FSM_stat)
                # print("Readout Request =", cReadouReq)
                print(" ")
                sleep(0.01)

            while cReadoutReq == 1:
                cNWords = self.hw.getNode("user.stat_regs.words_to_read").read()
                cNtriggers = self.hw.getNode("user.stat_regs.trigger_cntr").read()
                self.hw.dispatch()
                print("-----------------------------")
                print("Words to Read   =", cNWords)
                print("Trigger Counter =", cNtriggers)
                print(" ")

                # read data
                ddr3_data = self.hw.getNode("ddr3.fc7_daq_ddr3").readBlockOffset(cNWords.value(), self.ddr3_offset) #cNWords.value()
                self.hw.dispatch()

                data += ddr3_data

                self.ddr3_offset = 0

                cReadoutReq = self.hw.getNode("user.stat_regs.readout1.readout_req").read()
                self.hw.dispatch()
        else:
            cNWords = self.hw.getNode("user.stat_regs.words_to_read").read()
            self.hw.dispatch()
            cNEventsAvailable = cNWords.value() // cEventSize

            while cNEventsAvailable == 0:
                sleep(0.001)
                cNWords = self.hw.getNode("user.stat_regs.words_to_read").read()
                self.hw.dispatch()
                cNEventsAvailable = cNWords.value() // cEventSize

            size = cNEventsAvailable * cEventSize
            if self.ddr3_offset + size > 134217727:
                self.ddr3_offset = 0

            # read data
            data = self.hw.getNode("ddr3.fc7_daq_ddr3").readBlockOffset(cNWords.value(),0) #size, self.ddr3_offset
            self.hw.dispatch()

            self.ddr3_offset += size

        with open("raw_data_log.txt", "w+") as f:
            for i in range(len(data) // 4):
                line = "\t".join(["%.8x" % (x,) for x in data[i*4:(i+1)*4]])
                f.write(line + "\n")
                print(line)

        events = self.decode_events(data)
        json_like = pprint.pformat(events)
        print(json_like)
        with open("decoded_data_log.txt", "w+") as f:
            f.write(json_like)

        return data

    def ReadNEvents(pNEvents):
        WriteReg ("fc7_daq_cnfg.readout_block.packet_nbr", pNEvents-1);
        WriteReg ("fc7_daq_cnfg.readout_block.global.data_handshake_enable", 0x1);

        WriteReg ("fc7_daq_cnfg.fast_command_block.triggers_to_accept", pNEvents);
        WriteReg ("fc7_daq_ctrl.fast_command_block.control.load_config", 0x1);

        cReadoutReq = ReadReg ("fc7_daq_stat.readout_block.general.readout_req")
        while cReadoutReq == 0:
            sleep(0.001)
            cReadoutReq = ReadReg ("fc7_daq_stat.readout_block.general.readout_req")

        cNWords = ReadReg ("fc7_daq_stat.readout_block.general.words_cnt")

        data = self.hw.getNode("user.fc7_daq_ddr3").readBlockOffset(cNWords, self.ddr3_offset)
        self.hw.dispatch()

        self.ddr3_offset = 0
        return data

    def DDR3SelfTest(self):
        sleep(1)
        self.WriteReg("user.ctrl_regs.readout_block.ddr3_traffic_str", 1)
        sleep(0.001)
	# self.WriteReg("user.ctrl_regs.readout_block.ddr3_traffic_str", 0)
        cDDR3Checked = self.ReadReg("user.stat_regs.readout1.ddr3_self_check_done")
        # self.WriteReg("user.ctrl_regs.readout_block.ddr3_traffic_str", 0)
        # if not cDDR3Checked:
        #     print("Waiting for DDR3 to finish self-test")
        while not cDDR3Checked:
            sleep(0.001)
            cDDR3Checked = self.ReadReg("user.stat_regs.readout1.ddr3_self_check_done")
            Test_status = self.ReadReg ("user.stat_regs.stat_reg_29")
            print("FSM status      = ", Test_status)
            print("Self-Check done = ", cDDR3Checked)
            if cDDR3Checked:
                self.WriteReg("user.ctrl_regs.readout_block.ddr3_traffic_str", 0)

        num_errors = self.ReadReg("user.stat_regs.ddr3_num_errors")
        num_words = self.ReadReg("user.stat_regs.ddr3_num_words")
        print("num_words: " + str(num_words))
        print("num_errors: " + str(num_errors))
        if num_errors != 0:
            print("DD3 self-test FAILED")
        else:
            print("DD3 self-test PASSED")
        return num_errors == 0



##############################
### Optical Implementation ###
##############################

    def Read_lpgbt_clocks(self):
        lpgbt_rx_clk = self.ReadReg("user.stat_regs.stat_reg_21")
        lpgbt_tx_clk = self.ReadReg("user.stat_regs.stat_reg_22")
        print("lpGBT Rx Clock Rate (MHz) ="), lpgbt_rx_clk//1000
        print("lpGBT Tx Clock Rate (MHz) ="), lpgbt_tx_clk//1000

    def Reset_mgt_banks(self):
        self.hw.getNode("user.ctrl_regs.Optical_link.gbtbank_reset").write(1)
        sleep(0.1)
        self.hw.getNode("user.ctrl_regs.Optical_link.gbtbank_reset").write(0)
        # sleep(0.1)
        self.hw.dispatch()

    def Configure_nibble_order(self, enable):
        self.hw.getNode("user.ctrl_regs.Optical_link.reverse_nibble").write(enable)
        self.hw.dispatch()

    def Reset_prbs_cntr(self):
        self.hw.getNode("user.ctrl_regs.Optical_link.reset_prbs_cntr").write(1)
        sleep(0.01)
        self.hw.getNode("user.ctrl_regs.Optical_link.reset_prbs_cntr").write(0)
        self.hw.dispatch()

    def prbs_cntr_sel(self ,cntr_sel):
        self.hw.getNode("user.ctrl_regs.Optical_link.prbs_error_cntr_sel").write(cntr_sel)
        self.hw.dispatch()

    def prbs_rx_group_sel(self, rx_group_sel):
        self.hw.getNode("user.ctrl_regs.Optical_link.prbs_rx_group_sel").write(rx_group_sel)
        self.hw.dispatch()

    def Read_PRBS_cntr(self):
        return self.ReadReg("user.stat_regs.prbs_ber_cntr")

    def Read_PRBS_Frame_cntr(self):
        PRBS_cntr_lo = self.ReadReg("user.stat_regs.prbs_frame_cntr_low")
        PRBS_cntr_hi = self.ReadReg("user.stat_regs.prbs_frame_cntr_high")
        return PRBS_cntr_hi << 32 | PRBS_cntr_lo

    def prbs_emul_en(self, emulator_en):
        self.hw.getNode("user.ctrl_regs.Optical_link.prbs_emulator_en").write(emulator_en)
        self.hw.dispatch()

    def Set_prbs_frame_to_run(self, frames_to_run):
        self.hw.getNode("user.ctrl_regs.prbs_frames_to_run_low").write(frames_to_run &  0xFFFFFFFF)
        self.hw.getNode("user.ctrl_regs.prbs_frames_to_run_high").write(frames_to_run>> 32)
        self.hw.getNode("user.ctrl_regs.Optical_link.load_prbs_config").write(1)
        self.hw.getNode("user.ctrl_regs.Optical_link.load_prbs_config").write(0)
        self.hw.dispatch()

    def Start_prbs_test(self):
        self.hw.getNode("user.ctrl_regs.Optical_link.start_rpbs_checker").write(1)
        self.hw.getNode("user.ctrl_regs.Optical_link.start_rpbs_checker").write(0)
        self.hw.dispatch()

    def Stop_prbs_test(self):
        self.hw.getNode("user.ctrl_regs.Optical_link.stop_rpbs_checker").write(1)
        self.hw.getNode("user.ctrl_regs.Optical_link.stop_rpbs_checker").write(0)
        self.hw.dispatch()

    # lpGBT slow control
    def lpgbt_send_write_cmd(self,reg_address,reg_data):
        self.hw.getNode("user.ctrl_regs.Optical_link_cnfg1.ic_tx_fifo_din").write(reg_data)
        self.hw.getNode("user.ctrl_regs.Optical_link_cnfg1.ic_chip_addr_tx").write(0x70)#write(chip_addr)
        self.hw.getNode("user.ctrl_regs.Optical_link_cnfg2.ic_reg_addr_tx").write(reg_address)
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_tx_fifo_wr_en").write(1)
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_tx_fifo_wr_en").write(0)
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_send_wr_cmd").write(1)
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_send_wr_cmd").write(0)
        self.hw.dispatch()

    def lpgbt_send_read_cmd(self,reg_address):
        self.hw.getNode("user.ctrl_regs.Optical_link_cnfg1.ic_chip_addr_tx").write(0x70)
        self.hw.getNode("user.ctrl_regs.Optical_link_cnfg2.ic_reg_addr_tx").write(reg_address)
        self.hw.getNode("user.ctrl_regs.Optical_link_cnfg2.ic_nb_of_words_to_read_tx").write(1)
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_send_rd_cmd").write(1)
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_send_rd_cmd").write(0)
        self.hw.dispatch()

    # Reads data from FPGA's fifo after a read command has been send to lpGBT
    def lpgbt_read_fifo(self, verbose=False):
        chip_addr_rx = self.hw.getNode("user.stat_regs.lpgbt_1.ic_chip_addr_rx").read()
        reg_addr_rx  = self.hw.getNode("user.stat_regs.lpgbt_2.ic_reg_addr_rx").read()
        words_read   = self.hw.getNode("user.stat_regs.lpgbt_2.ic_nb_of_words_rx").read()
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_rx_fifo_rd_en").write(1)
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_rx_fifo_rd_en").write(0)
        fifo_dout = self.hw.getNode("user.stat_regs.lpgbt_1.ic_rx_fifo_dout").read()
        self.hw.dispatch()
        lpgbt_rx_fifo_empty = self.hw.getNode("user.stat_regs.lpgbt_1.ic_rx_empty").read()
        self.hw.dispatch()
        if verbose:
            print("Chip Address Rx      =", hex(chip_addr_rx))
            print("Reg Address Rx       =", reg_addr_rx)
            print("Nb of Words Received =", words_read)
            print("Fifo Readback Data   =", fifo_dout)
            print("Fifo Empty Flag      =", lpgbt_rx_fifo_empty)
        return fifo_dout

    # Sends a Read Command to lpGBT and returns the data received
    def read_lpgbt_reg(self, register_addr):
        self.lpgbt_send_read_cmd(register_addr)
        sleep(0.01)
        start = time.time()
        while True:
            sleep(0.001)
            empty = self.hw.getNode("user.stat_regs.lpgbt_1.ic_rx_empty").read()
            self.hw.dispatch()
            if not empty:
                break
            if time.time() - start > 1:
                print("ERROR: timeout on rx_fifo_empty")
                return 0
        return self.lpgbt_read_fifo()

    def lpgbt_rst_ic(self):
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_tx_reset").write(1)
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_rx_reset").write(1)
        sleep(0.1)
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_tx_reset").write(0)
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_rx_reset").write(0)
        self.hw.dispatch()

    def lpgbt_ic_status(self):
        lpgbt_ic_tx_ready = self.hw.getNode("user.stat_regs.lpgbt_1.ic_tx_ready").read()
        lpgbt_ic_rx_fifo_empty = self.hw.getNode("user.stat_regs.lpgbt_1.ic_rx_empty").read()
        self.hw.dispatch()
        print("lpGBT IC Tx Ready = ", lpgbt_ic_tx_ready)
        print("lpGBT IC Rx Ready = ", lpgbt_ic_rx_fifo_empty)

    def read_uplink (self):
        uplink_frame =  self.hw.getNode("user.stat_regs.uplink_frame").read()
        self.hw.dispatch()
        print("UpLink Frame Data = ", hex(uplink_frame))

    def lpgbt_loopback_en(self,enable):
        self.hw.getNode("user.ctrl_regs.Optical_link.ic_loopback_en").write(enable)
        self.hw.dispatch()

    def ConfigureCDCE(self, refclk_rate):
        print("\tConfiguring CDCE clock generator via SPI")
        spi_comm = 0x8fa38014; # command to SPI block
        # , // This clock is not used, but it can be used as another GBT clock (120 MHz, LVDS, phase shift 0 deg)
        # , // GBT clock reference: 120 MHz, LVDS, phase shift 0 deg (0xEB820321: 320 MHz, LVDS, phase shift 0 deg)
        # , // DDR3 clock reference: 240 MHz, LVDS, phase shift 0 deg
        # , // Not used (off)
        # , // Not used (off)
        # , // Reference selection: 0x10000EB5 secondary reference, 0x10000E75 primary reference
        # , // VCO selection: 0x030E02E6 select VCO1 if CDCE reference is 40 MHz, 0x030E02F6 CO selection: 0x030E02E6 select VCO1 if CDCE reference is 40 MHz, 0x030E02F6 select VCO2 if CDCE reference is > 40 MHz
        # , // RC network parameters
        #  //0x20009978  // Sync command configuration        # Original register values from "fc7_test_cdce.py".
        # This is being used as reference.
        wrbuffer = [0,1,2,3,4,5,6,7,8,0]
        wrbuffer[0] = 0xeb840320 # reg0 (out0=240mhz,lvds, phase shift  0deg)
        wrbuffer[1] = 0xeb020321 # reg1 (out1=160mhz,lvds, phase shift  0deg)
        wrbuffer[2] = 0xeb840302 # reg2 (out2=240mhz,lvds  phase shift  0deg)
        wrbuffer[3] = 0xeb840303 # reg3 (out3=240mhz,lvds, phase shift  0deg)
        wrbuffer[4] = 0xeb140334 # reg4 (out4= 40mhz,lvds, phase shift  0deg, r4.1=1)
        wrbuffer[5] = 0x113c0cf5 # reg5 (3.4ns lockw,lvds in, dc term, prim ref enable, sec ref enable, smartmux off, failsafe off etc.)
        wrbuffer[6] = 0x33041be6 # reg6 (vco1, ps=4, fd=12, fb=1, chargepump 50ua, internal filter, r6.20=0, auxout= enable; auxout= out2)
        wrbuffer[7] = 0xbd800df7 # reg7 (c2=473.5pf, r2=98.6kr, c1=0pf, c3=0pf, r3=5kr etc, sel_del2=1, sel_del1=1)
        wrbuffer[8] = 0x20009978 # trigger sync
        # # Override new register values to configure the desired frequencies.
        # Use 125MHz as PRI_REF. Synthesizes 120MHz at U1 and 240MHz at U2.
        if refclk_rate == 160:
            wrbuffer[1] = 0xeb020321 # 160 mhz reg1
            print("\tSetting mgt ref clock to 160MHz")
        elif refclk_rate == 320:
            wrbuffer[1] = 0xEB820321 # 320 mhz reg1
            print("\tSetting mgt ref clock to 320MHz")
        else:
            print("\t!!!Wrong ref clk rate - exitting")
            return
        wrbuffer[2] = 0xEB840302 # reg2
        wrbuffer[5] = 0x013c0cb5 # reg5
        # #wrbuffer[6] = 0x33041BE6 # reg6

        # Program registers via IPbus
        for i in range(0,9):
            self.WriteReg("system.spi.tx_data",wrbuffer[i])
            self.WriteReg("system.spi.command", spi_comm)
            self.ReadReg("system.spi.rx_data") # dummy read
            self.ReadReg("system.spi.rx_data") # dummy read

    def SynchronizeCDCE(self):
        print("\tCDCE Synchronization")
        print("\t\tDe-Asserting Sync")
        self.WriteReg("system.ctrl.cdce_sync", 0)
        print("\t\tAsserting Sync")
        self.WriteReg("system.ctrl.cdce_sync", 1)
        # # waiting for sync
        # while( self.ReadReg("system.stat_cdce.sync_done") != 1 ):
        #     print("\t\t\tWaiting for sync")
        #     sleep(1)
        # # getting status
        # print("\tCDCE Status:")
        # print("\t\tsync busy :", self.ReadReg("system.stat_cdce.sync_busy"))
        # print("\t\tsync done :", self.ReadReg("system.stat_cdce.sync_done"))

    def CDCEStoreToEEPROM(self):
        print("\tStoring Configuration in EEPROM")
        spi_comm = 0x8FA38014; # command to spi block
        cdce_write_to_eeprom_unlocked = 0x0000001F # write eeprom

        self.WriteReg("system.spi.tx_data", cdce_write_to_eeprom_unlocked)
        self.WriteReg("system.spi.command", spi_comm)
        self.ReadReg("system.spi.rx_data") # dummy read
        self.ReadReg("system.spi.rx_data") # dummy read
