# Instalation guide for lpgbt-rd53a-bert dependencies

## Install Ph2_ACF

- Taken from https://gitlab.cern.ch/cms_tk_ph2/Ph2_ACF
- Dependencies:

```
sudo yum install epel-release
sudo yum install pugixml-devel
sudo yum install boost-devel
```

- Install root or take it from cvmfs

```
source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.20.08/x86_64-centos7-gcc48-opt/bin/thisroot.sh
```
- Install ipbus (from https://ipbus.web.cern.ch/doc/user/html/software/install/yum.html)

```
sudo curl https://ipbus.web.cern.ch/doc/user/html/_downloads/ipbus-sw.centos7.x86_64.repo -o /etc/yum.repos.d/ipbus-sw.repo
sudo yum clean all  
sudo yum groupinstall uha
```

- Install Ph2_ACF

```
git clone https://gitlab.cern.ch/cmsinnertracker/Ph2_ACF.git
cd Ph2_ACF; source setup.sh; mkdir myBuild; cd myBuild; cmake ..; make -j4; cd ..
```

## Install Adafruit libraries

- Adapted  from https://github.com/swertz/Adafruit_Python_GPIO/tree/lpGBT

```
    sudo yum install doxygen
    sudo yum install libusb-devel
    sudo yum install libconfuse-devel
    sudo yum install swig python-devel
    sudo yum install epel-release boost-filesystem boost-thread
    sudo yum install boost-devel
    sudo yum install python-pip i2c-tools-python
```

  - Install libftdi library

```
    wget https://www.intra2net.com/en/developer/libftdi/download/libftdi1-1.4.tar.bz2
    tar xjf libftdi1-1.4.tar.bz2; rm -rf libftdi1-1.4.tar.bz2
    cd libftdi1-1.4/
    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX="/usr/" -DPYTHON_INCLUDE_DIR="/usr/include/python2.7" -DPYTHON_LIBRARIES="/usr/lib/python2.7/" ../
    make -j 4
    sudo make install
```

- Install Adafruit libraries

```
    cd ../../
    git clone --single-branch --branch lpGBT https://github.com/swertz/Adafruit_Python_GPIO.git
    cd Adafruit_Python_GPIO
    sudo python setup.py install
```