#!/usr/bin/env python

from __future__ import division, print_function, absolute_import

import argparse
from lpGBT_tools import LpGBT

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Configure lpGBT')
    
    parser.add_argument('-i', '--interface', choices=['i2c', 'ic'], required=True, help='Configuration interface')
    
    parser.add_argument('--dll', action='store_true', help='Configure DLL and intialize lpGBT')
    parser.add_argument('--pll', action='store_true', help='Configure PLL and wait for DLL config (always uses I2C no matter what!)')

    parser.add_argument('-u', '--up', choices=range(7), nargs='*', type=int, help='Enable uplink groups (0-6)')
    parser.add_argument('-d', '--down', choices=range(4), nargs='*', type=int, help='Enable downlink groups (0-3)')

    parser.add_argument('--disable-up', choices=range(7), nargs='*', type=int, help='Disable uplink groups (0-6)')
    parser.add_argument('--disable-down', choices=range(4), nargs='*', type=int, help='Disable downlink groups (0-3)')
    
    parser.add_argument('--current', type=int, default=3, help='Specify current for downlink (default 3, range: 1-7) (specify when enabling downlink)')
    parser.add_argument('--invert-down', action='store_true', help='Invert downlink (specify when enabling downlink)')
    parser.add_argument('--preemph-down', type=int, default=0, help='Use pre-emphasis on downlink (default: 0, range: 0-7) (specify when enabling downlink)')
    
    parser.add_argument('--invert-up', action='store_true', help='Invert uplink (specify when enabling uplink)')
    parser.add_argument('--disable-term', action='store_true', help='Disable uplink 100 ohm termination (specify when enabling uplink)')
    parser.add_argument('--disable-bias', action='store_true', help='Disable uplink AC bias (specify when enabling uplink)')
    parser.add_argument('--phase', choices=range(15), type=int, help='Uplink phase shift (specify when enabling uplink)')
    parser.add_argument('--equal', choices=range(4), type=int, help='Uplink equalization level (specify when enabling uplink)')
    parser.add_argument('--phase-mode', choices=range(4), type=int, help='Uplink phase selection mode (specify when enabling uplink)')
    
    parser.add_argument('--find-phases', choices=range(7), type=int, nargs='*', help='Find best uplink phase using quick BERT (specify uplink groups to tune). Make sure the groups are enabled!')

    parser.add_argument('-s', '--status', action='store_true', help='Print status and exit')

    args = parser.parse_args()

    lpGBT = LpGBT(interface=args.interface)

    if args.dll:
        lpGBT.configDLL()
    if args.pll:
        lpGBT.configPLLI2C()

    if args.status:
        print("LpGBT FSM status: {}".format(lpGBT.getStatus()))

    if args.up:
        up_args = {}
        if args.invert_up:
            up_args['invert'] = True
        if args.phase:
            up_args['phase'] = args.phase
        if args.equal:
            up_args['equal'] = args.equal
        if args.phase_mode:
            up_args['phaseMode'] = args.phase_mode
        if args.disable_term:
            up_args['enableTerm'] = False
        if args.disable_bias:
            up_args['enableBias'] = False
        for gr in args.up:
            print("Enabling uplink group {} with options {}".format(gr, up_args))
            lpGBT.configUpLink(gr, **up_args)
    
    if args.down:
        down_args = {}
        if args.invert_down:
            down_args['invert'] = args.invert_down
        down_args['current'] = args.current
        if args.preemph_down:
            down_args['preemph'] = args.preemph_down
        for gr in args.down:
            print("Enabling downlink group {} with options {}".format(gr, down_args))
            lpGBT.configDownLink(gr, **down_args)
    
    if args.disable_up:
        for gr in args.disable_up:
            print("Disabling uplink group {}".format(gr))
            lpGBT.disableUpLink(gr)

    if args.disable_down:
        for gr in args.disable_down:
            print("Disabling downlink group {}".format(gr))
            lpGBT.disableDownLink(gr)

    if args.find_phases:
        for gr in args.find_phases:
            lpGBT.findPhase(gr, verbose=True)
