#!/usr/bin/env python

from __future__ import division, print_function, absolute_import

import os
import argparse
import json
import numpy as np
import datetime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable

try:
    from lpGBT_tools import LpGBT
    import RD53A_tools as RD53A
    import libpyDTC.fc7_daq as FC7
except:
    print("Warning: could not import lpGBT, FC7_DAQ and RD53A... I can only plot stuff!")

def parseFixString(fix):
    """Parses string of the type "TAP0=1,TAP2=2", returns dict with tap name as key and value as value"""

    fixedparams = (p for p in fix.split(',') if len(p) > 1)
    fixParamValues = {}
    for p in fixedparams:
        paramToFix = p.split('=')[0]
        fixVal = int(p.split('=')[1])
        fixParamValues[paramToFix] = fixVal
    return fixParamValues

class BERTScan(object):
    def __init__(self, mode, interface, up, time, connection, fix, findPhase, findPhaseOnce, verbose):
        self.mode = mode
        self.interface = interface
        self.up = up
        self.time = time
        self.connection = connection
        self.fix = fix
        self.findPhase = findPhase
        self.findPhaseOnce = findPhaseOnce
        self.verbose = verbose

        self.ranges = {
                # 'PHASE': range(0, 15), # setting 15 is invalid
                'PHASE': range(0, 8), # restrict to first "eye"
                'EQ': range(0, 4),
                'TAP0': range(500, 1050, 250),
                'TAP1': range(0, 170, 20),
                'TAP2': range(0, 100, 10),
            }
        self.params = ['PHASE', 'EQ', 'TAP0', 'TAP1', 'TAP2']

        if self.findPhase:
            self.params.remove('PHASE')
            self.ranges.pop('PHASE')

        self.lpGBT = LpGBT(self.interface)

        if self.findPhaseOnce:
            phase, bestBER = self.lpGBT.findPhase(self.up, verbose=self.verbose)
            if bestBER > 0.1:
                raise Exception("Warning: could not determine a sensible phase...")
            self.ranges['PHASE'] = [phase]

        for paramToFix, fixVal in parseFixString(fix).items():
            self.ranges[paramToFix] = [fixVal]
            if self.verbose:
                print("Fixing {paramToFix} to {fixVal}".format(**locals()))
        self.scanAxes = [ self.ranges[p] for p in self.params ]

        if self.mode == 'lpgbt':
            self.lpGBT.configBERT(group=self.up)
        elif self.mode == 'fpga':
            FC7.sel_prbs_rx_group(self.up)

    def scan(self):
        datetime_start = '{:%Y-%m-%d_%H-%M}'.format(datetime.datetime.now())

        scanShape = [len(x) for x in self.scanAxes]
        scanIdxs = np.ndindex(*scanShape)
        berResults = np.zeros(scanShape, dtype=float)

        print("Starting scans")

        iScan = 0
        nScans = np.prod(berResults.shape)

        for idx in scanIdxs:
            iScan += 1

            if self.findPhase:
                phase, bestBER = self.lpGBT.findPhase(self.up, verbose=self.verbose)
                if bestBER > 0.1:
                    print("Warning: could not determine a sensible phase...")
                    berResult[idx] = bestBER
                    continue
            else:
                phase = self.scanAxes[self.params.index('PHASE')][idx[self.params.index('PHASE')]]
                # FIXME - see https://lpgbt.web.cern.ch/lpgbt/manual/known_issues.html#eportrx-phases-swap
                if phase == 2:
                    phase = 3
                elif phase == 3:
                    phase = 2
            eq = self.scanAxes[self.params.index('EQ')][idx[self.params.index('EQ')]]
            tap0 = self.scanAxes[self.params.index('TAP0')][idx[self.params.index('TAP0')]]
            tap1 = self.scanAxes[self.params.index('TAP1')][idx[self.params.index('TAP1')]]
            tap2 = self.scanAxes[self.params.index('TAP2')][idx[self.params.index('TAP2')]]

            print("Measuring BER for phase = {}, equalization = {}, TAP0 = {}, TAP1 = {}, TAP2 = {}".format(phase, eq, tap0, tap1, tap2))
            self.lpGBT.configUpLink(group=self.up, phase=phase, equal=eq)
            RD53A.setTap0(tap0)
            RD53A.setTap1(tap1)
            RD53A.setTap2(tap2)

            try:
                if self.mode == 'lpgbt':
                    ber = self.lpGBT.runBERT(self.time, self.verbose)
                elif self.mode == 'fpga':
                    ber = FC7.runBERT(self.time, self.verbose)
            except:
                print("Warning: didn't get any data!")
                ber = 1.
            berResults[idx] = ber
            print('{:3.0f}% completed!'.format(100.*(iScan)/nScans))
        
        data = {
            'connection': self.connection,
            'nbits': 2**(5 + 2 * self.time) * 32,
            'time': self.time,
            'params': self.params,
            'values': self.scanAxes,
            'ber': berResults,
            'uplink': self.up,
            'checker': self.mode,
            'datetime-start': datetime_start,
            'datetime-stop': '{:%Y-%m-%d_%H-%M}'.format(datetime.datetime.now())
        }

        return data

class Plotter(object):
    plotCfg = [
        # "key" is used to get the correct array in the "data" dictionary
        { 'key': 'ber', 'ylabel': 'BER', 'title': 'Bit error rate', 'scale': 1, 'logy': True },
    ]

    def __init__(self, data, params, fix, prefix):
        self.data = data
        self.fixParams = parseFixString(fix)
        self.prefix = prefix
        assert(len(params) <= 2)
        self.nDim = len(params)
        for p in params:
            assert(p not in self.fixParams.keys())
        self.params = params

    def plot(self):
        #### 1-D plots ####
        if self.nDim == 1:
            paramX = self.params[0]
            paramXIdx = self.data['params'].index(paramX)
            xValues = self.data['values'][paramXIdx]

            otherParams = self.data['params'][:]
            otherParams.remove(paramX)
            fixStrings = []
            otherParamValues = []

            for other in otherParams:
                otherIdx = self.data['params'].index(other)
                if other in self.fixParams.keys():
                    otherVals = [ self.fixParams[other] ]
                    fixStrings.append('{}={}'.format(other, otherVals[0]))
                else:
                    otherVals = self.data['values'][otherIdx]
                otherParamValues.append(otherVals)

            for cfg in self.plotCfg:
                values = self.data[cfg['key']]
                # when loaded from JSON we have lists, not numpy arrays
                if isinstance(values, list):
                    values = np.asarray(values)
                values *= cfg.get('scale', 1.)

                valuesList = []
                valuesLegends = []
                
                otherValsMesh = np.meshgrid(*otherParamValues, indexing='ij')
                for idx in np.ndindex(*otherValsMesh[0].shape):
                    m_slice = [slice(None)] * len(self.data['params'])
                    for i,j in enumerate(idx):
                        m_slice[self.data['params'].index(otherParams[i])] = slice(j, j+1)
                    valuesList.append(np.ravel(values[tuple(m_slice)]))
                    valuesLegends.append(', '.join(
                        ["{}={}".format(otherParams[i], otherValsMesh[i][idx]) for i in range(len(idx)) if len(otherParamValues[i]) > 1]))
                        
                text = ', '.join(["{}={}".format(otherParams[i], otherValsMesh[i][idx]) for i in range(len(idx)) if len(otherParamValues[i]) == 1])
                text += (', ' if text else '') + '{:.2e} bits'.format(self.data['nbits'])

                fileName = self.prefix + '_' + cfg['key'] + '.png'
                fileName = fileName.replace('DATE', self.data['datetime-stop'])
                fileName = fileName.replace('XX', paramX)
                fileName = fileName.replace('FF', '_'.join(fixStrings)).replace('CC', self.data['connection'])

                # do the actual plot
                self._plot1D(cfg, np.array(valuesList), valuesLegends, paramX, xValues, fileName, text, minVal=1./self.data['nbits'])

        #### 2-D plots ####
        elif self.nDim == 2:
            paramX = self.params[0]
            paramY = self.params[1]
            if paramX == paramY:
                raise Exception("Specify two different parameters to plot in 2D")

            paramXIdx = self.data['params'].index(paramX)
            xValues = self.data['values'][paramXIdx]
            paramYIdx = self.data['params'].index(paramY)
            yValues = self.data['values'][paramYIdx]

            otherParams = self.data['params'][:]
            otherParams.remove(paramX)
            otherParams.remove(paramY)
            otherParamValues = []

            for other in otherParams:
                otherIdx = self.data['params'].index(other)
                if other in self.fixParams.keys():
                    otherVals = [ self.fixParams[other] ]
                else:
                    otherVals = self.data['values'][otherIdx]
                otherParamValues.append(otherVals)

            for cfg in self.plotCfg:
                values = self.data[cfg['key']]

                # when loaded from JSON we have lists, not numpy arrays
                if isinstance(values, list):
                    values = np.asarray(values)
                values *= cfg.get('scale', 1.)
                
                fileName = self.prefix + '_' + cfg['key'] + '.png'
                fileName = fileName.replace('DATE', self.data['datetime-stop'])
                fileName = fileName.replace('XX', paramX).replace('YY', paramY)
                fileName = fileName.replace('CC', self.data['connection'])

                if otherParamValues:
                    otherValsMesh = np.meshgrid(*otherParamValues, indexing='ij')
                    for idx in np.ndindex(*otherValsMesh[0].shape):
                        m_slice = [slice(None)] * len(self.data['params'])
                        for i,j in enumerate(idx):
                            m_slice[self.data['params'].index(otherParams[i])] = slice(j, j+1)
                        theseValues = np.squeeze(values[tuple(m_slice)])
                        if paramXIdx > paramYIdx:
                            theseValues = theseValues.T

                        fixStrings = [ "{}={}".format(otherParams[i], otherValsMesh[i][idx]) for i in range(len(idx))]
                        valuesLegends = ', '.join(fixStrings) + ', {:.2e} bits'.format(self.data['nbits'])

                        thisFileName = fileName.replace('FF', '_'.join(fixStrings))

                        # do the actual plot
                        self._plot2D(cfg, theseValues, paramX, xValues, paramY, yValues, thisFileName, valuesLegends, minVal=1./self.data['nbits'])
                else:
                    valuesLegends = '{:.2e} bits'.format(self.data['nbits'])
                    self._plot2D(cfg, values, paramX, xValues, paramY, yValues, fileName, valuesLegends, minVal=1./self.data['nbits'])
        else:
            raise Exception("Can only plot in 1D and 2D")

    def _plot1D(self, cfg, valuesList, valuesLegends, paramX, xValues, fileName, text=None, minVal=None):
        figs, ax = plt.subplots(figsize=(9,4))
        if cfg.get('logy', False):
            ax.set_yscale("log")

        nLines = len(valuesList)

        cm = plt.cm.get_cmap('viridis', nLines)
        cNorm  = colors.Normalize(vmin=0, vmax=nLines)
        scalarMap = plt.cm.ScalarMappable(norm=cNorm, cmap=cm)

        for i in range(nLines):
            colorVal = scalarMap.to_rgba(i)
            ax.plot(xValues, valuesList[i], 'x--', label=valuesLegends[i], color=colorVal)

        if minVal:
            ax.plot(xValues, [minVal]*len(xValues), ':', color='0.5')

        ax.set_xlabel(paramX)
        ax.set_ylabel(cfg['ylabel'])
        ax.set_title(cfg['title'])
        if cfg.get('logy', False):
            ax.set_ylim(ymin=np.min(valuesList[valuesList > 0]) / 2)

        if text:
            plt.gcf().text(0.01, 0.94, text, fontsize=11)

        # FIXME
        if nLines < 20:
            ax.legend()
        plt.tight_layout()

        outDir = os.path.dirname(fileName)
        if not os.path.isdir(outDir) and outDir != "":
            os.makedirs(outDir)

        figs.savefig(fileName)
        plt.close(figs)

    def _plot2D(self, cfg, values, paramX, xValues, paramY, yValues, fileName, text=None, minVal=None):
        figs, ax = plt.subplots(figsize=(12,4))
        im = ax.imshow(values.T, aspect='auto', cmap=plt.cm.get_cmap('Blues', 100), origin='lower')

        divider = make_axes_locatable(ax)
        cax = divider.append_axes('right', size='5%', pad=0.05)
        cbar = plt.colorbar(im, cax=cax)
        cbar.ax.set_ylabel(cfg['ylabel'])

        ax.set_xlabel(paramX)
        ax.set_ylabel(paramY)
        ax.set_xticks(np.arange(len(xValues)))
        ax.set_yticks(np.arange(len(yValues)))
        ax.set_xticklabels(xValues)
        ax.set_yticklabels(yValues)
        ax.set_title(cfg['title'])
        plt.tight_layout()

        for i in range(len(xValues)):
            for j in range(len(yValues)):
                val = values[i, j]
                if val > 0.6 * np.max(values):
                    color = 'white'
                else:
                    color = 'black'
                if minVal and val == minVal:
                    color = '#008000'
                ax.text(i, j, '{:.1e}'.format(val) if val else '0', ha='center', va='center', color=color)

        if text:
            plt.gcf().text(0.01, 0.94, text, fontsize=11)

        outDir = os.path.dirname(fileName)
        if not os.path.isdir(outDir) and outDir != "":
            os.makedirs(outDir)

        figs.savefig(fileName)
        plt.close(figs)


def saveJSON(data, outPath):
    class NumpyEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, np.ndarray):
                return obj.tolist()
            return json.JSONEncoder.default(self, obj)

    outDir = os.path.dirname(outPath)
    if not os.path.isdir(outDir) and outDir != "":
        os.makedirs(outDir)

    with open(outPath, 'w') as _f:
        json.dump(data, _f, cls=NumpyEncoder)

def loadJSON(path):
    with open(path) as _f:
        return json.load(_f)

def main():
    parser = argparse.ArgumentParser(description='Scan phase and EQ')
    parser.add_argument('-v', '--verbose', action='store_true')

    subparsers = parser.add_subparsers(dest='command')

    parser_scan = subparsers.add_parser('scan', help='Scan over phase and equalizations values')
    parser_scan.add_argument('-i', '--interface', choices=['ic', 'i2c'], default='i2c', help='Choose interface for lpGBT config')
    parser_scan.add_argument('-t', '--time', type=int, default=0, help='Time of the BERTs (check 2**(5+2*time) frames of 32 bits). Max. time is 15 for lpGBT, 29 for FPGA.')
    parser_scan.add_argument('-m', '--mode', choices=['lpgbt', 'fpga'], default='fpga', help='Use the PRBS checker on the lpGBT or on the FPGA')
    parser_scan.add_argument('-u', '--up', type=int, default=6, help='Uplink group to use')
    parser_scan.add_argument('-c', '--connection', type=str, default='', help="Type of connection measured: e.g. 'direct', 'roundboard', 'elink name'")
    parser_scan.add_argument('-o', '--output', help='Output prefix (incl. folder) for data. "DATE" will be replaced by datetime at which the scan finished, "CC" by connection type.')
    parser_scan.add_argument('--fix', default='', help='Optionally fix one or more parameters to a given value, e.g. PHASE=1,EQ=0.')
    parser_scan.add_argument('--find-phase', action='store_true', help='Automatically find best phase using quick BERT')
    parser_scan.add_argument('--find-phase-once', action='store_true', help='Automatically find best phase using quick BERT ONCE at the beginning, use that phase for whole scan')

    parser_plot = subparsers.add_parser('plot', help='Produce plot after running scan')
    parser_plot.add_argument('-p', '--params', choices=['PHASE', 'EQ', 'TAP0', 'TAP1', 'TAP2'], nargs='+', help='Choose parameters to plot on X axes (for 1D plot) or X and Y axes (for 2D plots)')
    parser_plot.add_argument('-i', '--input', help='Input JSON file with scan results to plot')
    parser_plot.add_argument('-o', '--output', default='./DATE_CC_XX_YY_FF', help='Output prefix (incl. folder) for plots. "DATE" will be replaced by datetime at which the scan finished, "XX" by x-scanned value, "YY" by y-scanned value, "FF" by fixed value, "CC" by connection type.')
    parser_plot.add_argument('-f', '--fix', default='', help='Fix not plotted parameters to given value')

    args = parser.parse_args()

    if args.command == 'scan':
        scanner = BERTScan(mode=args.mode, interface=args.interface, up=args.up, time=args.time, connection=args.connection, fix=args.fix, findPhase=args.find_phase, findPhaseOnce=args.find_phase_once, verbose=args.verbose)
        data = scanner.scan()

        def formatFileName(fileName):
            fileName = fileName.replace('DATE', data['datetime-stop'])
            fileName = fileName.replace('CC', args.connection)
            return fileName + '.json'

        if args.output:
            saveJSON(data, formatFileName(args.output))

    if args.command == 'plot':
        data = loadJSON(args.input)

        plotter = Plotter(data, args.params, args.fix, args.output)
        plotter.plot()

if __name__ == "__main__":
    main()
