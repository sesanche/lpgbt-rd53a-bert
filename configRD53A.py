#!/usr/bin/env python

from __future__ import division, print_function, absolute_import

import argparse
import RD53A_tools as RD53A

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Configure RD53A')

    parser.add_argument('-l', '--lanes', choices=range(4), nargs='*', type=int, help='Lanes to enable (0-3)')
    parser.add_argument('-t', '--enable-taps', choices=[1, 2], nargs='*', type=int, help='Enable taps (1 or 2) (mandatory to also specify lanes to enable!)')
    parser.add_argument('-i', '--invert-taps', choices=[1, 2], nargs='*', type=int, help='Invert taps (1 or 2) (mandatory to also specify lanes to enable!)')

    parser.add_argument('--tap0', type=int, help='Specify TAP0 value (0-1023, chip default is 500)')
    parser.add_argument('--tap1', type=int, help='Specify TAP1 value (0-1023, chip default is 0)')
    parser.add_argument('--tap2', type=int, help='Specify TAP2 value (0-1023, chip default is 0)')
    
    parser.add_argument('-d', '--data', choices=range(4), nargs='*', type=int, help='Specify type of data to send on each lane: 0 = clock, 1 = Aurora, 2 = PRBS, 3 = ground. If --lanes is specified and N lanes are enabled, give N numbers to configure those lanes. If --lanes is not specified, give 4 numbers (that will be used for lanes 0-3).')

    args = parser.parse_args()

    if args.lanes:
        assert(len(args.lanes) <= 4)
        enableTap1 = (1 in args.enable_taps) if args.enable_taps else False
        enableTap2 = (2 in args.enable_taps) if args.enable_taps else False
        invertTap1 = (1 in args.invert_taps) if args.invert_taps else False
        invertTap2 = (2 in args.invert_taps) if args.invert_taps else False
        print("Enabling lanes {}".format(", ".join([str(i) for i in args.lanes])))
        print("Tap configuration: ")
        print("- tap 1 {}enabled, {}inverted".format("" if enableTap1 else "not ", "" if invertTap1 else "not "))
        print("- tap 2 {}enabled, {}inverted".format("" if enableTap2 else "not ", "" if invertTap2 else "not "))
        RD53A.configureCML(args.lanes, enableTap1, enableTap2, invertTap1, invertTap2)

    if args.tap0:
        print("Setting TAP0 to {}".format(args.tap0))
        RD53A.setTap0(args.tap0)
    if args.tap1:
        print("Setting TAP1 to {}".format(args.tap1))
        RD53A.setTap1(args.tap1)
    if args.tap2:
        print("Setting TAP2 to {}".format(args.tap2))
        RD53A.setTap2(args.tap2)

    if args.data:
        if args.lanes:
            if len(args.data) != len(args.lanes):
                raise Exception("Give as many values for --data as you're enabling lanes")
            data = [0] * 4
            for i, l in enumerate(args.lanes):
                data[l] = args.data[i]
        else:
            data = args.data
        print("Configuring data lanes 0-3 with data type {}".format(data))
        RD53A.configureLanes(*data)
